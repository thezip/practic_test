var canvas = document.getElementById('graf');
var ctx = canvas.getContext('2d');

ctx.fillStyle = 'red';
ctx.fillRect(50,50,5,5);

ctx.rect(150, 200, 50, 50);
ctx.strokeStyle = 'blue';
ctx.lineWidth = '5';
ctx.stroke();
ctx.fillStyle = 'coral';
ctx.fill();

// LINE

ctx.moveTo(340, 125);
ctx.lineTo(155, 428);
ctx.stroke();

ctx.beginPath();
ctx.strokeStyle = 'darkmagenta';
ctx.moveTo(155, 428);
ctx.lineTo(100, 120);
ctx.stroke();

// Clear Canvas
ctx.clearRect(0, 0, 500, 500);

// Triangle
ctx. beginPath();
ctx.moveTo(50, 150);
ctx.lineTo(150, 50);
ctx.lineTo(200, 150);
// ctx.lineTo(50, 150);

ctx.fillStyle = 'greenyellow';
ctx.closePath();
ctx.stroke();
ctx.fill();