var canvas = document.getElementById('graf');
var ctx = canvas.getContext('2d');


var x1 = 240,
	y1 = 50,
	x2 = 450,
	y2 = 300,
	x3 = 100,
	y3 = 400;

function random(n){
	return Math.ceil(Math.random() * n);
}

	// (x, y); s - size; c - color
function drawPoint(x, y, s, c){
	ctx.fillStyle = c;
	ctx.fillRect(x - s / 2, y - s / 2, s, s);
}

drawPoint(x1, y1, 10, 'red');
drawPoint(x2, y2, 10, 'red');
drawPoint(x3, y3, 10, 'red');

var x = random(500),
	y = random(500);

drawPoint(x, y, 3, 'red');

for(var i = 0; i < 2000; i++){
	a = random(6);
	if (a == 1 || a == 2){
		x = (x1 + x) / 2;
		y = (y1 + y) / 2;
		drawPoint(x, y, 3, 'black');
	}else if (a == 3 || a == 4){
		x = (x2 + x) / 2;
		y = (y2 + y) / 2;
		drawPoint(x, y, 3, 'black');
	} else{
		x = (x3 + x) / 2;
		y = (y3 + y) / 2;
		drawPoint(x, y, 3, 'black');
	}
	
}

